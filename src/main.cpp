#include <iostream>

#include "ros/ros.h"
#include "std_msgs/Float64.h"


int main(int argc, char **argv) {
  std::cout << "Hello World!\n";

  ros::init(argc, argv, "my_node");

  ros::NodeHandle n;

  ros::Publisher right_wheelPub = n.advertise<std_msgs::Float64>("right_wheel_controller/command", 1000);

ros::Publisher left_wheelPub = n.advertise<std_msgs::Float64>("left_wheel_controller/command", 1000);

  while(ros::ok()) {
int in;
std_msgs::Float64 velocity;

 std::cout << "Please enter 0 for moving right wheel, 1 for left wheel or 2 for moving both wheels:";
std::cin >> in;

std::cout << "Please enter velocity:";

std::cin >> velocity.data;

switch (in) {

case 0:
right_wheelPub.publish(velocity);
break;
case 1:
left_wheelPub.publish(velocity);
break;
case 2:
right_wheelPub.publish(velocity);
left_wheelPub.publish(velocity);
break;
}
}
  return 0;
}
